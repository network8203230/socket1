import socket

def server_program():
    host = '127.0.0.1'
    port = 5000

    server_socket = socket.socket()
    server_socket.bind((host, port))

    server_socket.listen(2)
    conn, address = server_socket.accept()
    print("Connection from: " + str(address))

    number = 1
    while number <= 100:
        getData = conn.recv(1024).decode()
        print("From connected user: " + str(getData))
        number = int(getData) + 1
        conn.send(str(number).encode())

    conn.close()

if __name__ == '__main__':
    server_program()

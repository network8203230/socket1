import socket

def client_program():
    host = '127.0.0.1'
    port = 5000

    client_socket = socket.socket()
    client_socket.connect((host, port))

    number = 1
    while number <= 100:
        client_socket.send(str(number).encode())
        getData = client_socket.recv(1024).decode()
        print('Received from server: ' + getData)
        number = int(getData) + 1

    client_socket.close()

if __name__ == '__main__':
    client_program()
